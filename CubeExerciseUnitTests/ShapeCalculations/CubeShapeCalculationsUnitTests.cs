﻿using System;
using CubeExerciseCommon.Structs;
using CubeExerciseServiceLayer.Models.ShapeModels;
using NUnit.Framework;

namespace CubeExerciseUnitTests.ShapeCalculations
{
    [TestFixture]
    public class CubeShapeCalculationsUnitTests
    {
        
        [Test]
        public void CalculateCubeSurfaceArea_Given_NullModel_Returns_Int0()
        {
            var shapeModel = new CubeSl(new Position(), new Dimensions(), new Guid());
            const int expectedSurfaceAreaValue = 0;
            Assert.AreEqual(shapeModel.SurfaceArea, expectedSurfaceAreaValue);
        }
        
        [Test]
        public void CalculateCubeSurfaceArea_Given_Dimensions_Height0_Length0_Width0_Returns_Int0()
        {
            var shapeModel = new CubeSl(new Position(), new Dimensions
            {
                Height = 0,
                Length = 0,
                Width = 0
            }, new Guid());
            const int expectedSurfaceAreaValue = 0;
            Assert.AreEqual(shapeModel.SurfaceArea, expectedSurfaceAreaValue);
        }
        
        [Test]
        public void CalculateCubeSurfaceArea_Given_Dimensions_Length10_Height10_Width10_Returns_Double600()
        {
            var shapeModel = new CubeSl(new Position(), new Dimensions
            {
                Length = 10,
                Height = 10,
                Width = 10
            }, new Guid());
            const int expectedSurfaceAreaValue = 600;
            Assert.AreEqual(shapeModel.SurfaceArea, expectedSurfaceAreaValue);
        }
        
        [Test]
        public void CalculateVolume_Given_NullModel_Returns_Double0()
        {
            var shapeModel = new CubeSl(new Position(), new Dimensions(), new Guid());
            const int expectedVolumeArea = 0;
            Assert.AreEqual(shapeModel.Volume, expectedVolumeArea);
        }
        
        [Test]
        public void CalculateVolume_Given_Dimensions_Height0_Length0_Width0_Returns_Double0()
        {
            var shapeModel = new CubeSl(new Position(), new Dimensions
            {
                Height = 0,
                Length = 0,
                Width = 0
            }, new Guid());
            const int expectedVolumeArea = 0;
            Assert.AreEqual(shapeModel.Volume, expectedVolumeArea);
        }
        
        [Test]
        public void CalculateVolume_Given_Dimensions_Length10_Height10_Width10_Returns_Double1000()
        {
            var shapeModel = new CubeSl(new Position(), new Dimensions
            {
                Length = 10,
                Height = 10,
                Width = 10
            }, new Guid());
            const int expectedVolumeArea = 1000;
            Assert.AreEqual(shapeModel.Volume, expectedVolumeArea);
        }
    }
}