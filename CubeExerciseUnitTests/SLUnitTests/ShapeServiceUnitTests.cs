﻿using System;
using AutoMapper;
using CubeExerciseCommon.Enums;
using CubeExerciseCommon.Structs;
using CubeExerciseDAL.Interfaces;
using CubeExerciseDAL.Models;
using CubeExerciseServiceLayer.Infrastructure;
using CubeExerciseServiceLayer.Models.DTO;
using CubeExerciseServiceLayer.Models.ShapeModels;
using CubeExerciseServiceLayer.Services;
using Moq;
using NUnit.Framework;

namespace CubeExerciseUnitTests.SLUnitTests
{
    [TestFixture]
    public class ShapeServiceUnitTests
    {
        private readonly IMapper _mapper;
        private readonly Mock<IShapesRepository> _shapesRepository;

        public ShapeServiceUnitTests()
        {
            var myProfile = new SlMapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _mapper = new Mapper(configuration);
            _shapesRepository = new Mock<IShapesRepository>();
        }

        [Test]
        public void AddNewShape_Given_EmptyObject_And_ShapeTypeCube_Returns_Exception()
        {
            var cubeExerciseDbContextMoq = new Mock<IShapesRepository>();
            var shapeService = new ShapeService(cubeExerciseDbContextMoq.Object, _mapper);
            var shapeModel = new ShapeDtoSl();
            cubeExerciseDbContextMoq.Setup(x => x.Add(It.IsAny<Shape>())).Returns(1);
            Assert.Catch<Exception>(() => { shapeService.AddNewShape(shapeModel); });
        }

        [Test]
        public void AddNewShape_Given_null_And_ShapeTypeCube_Returns_Exception()
        {
            var shapeService = new ShapeService(_shapesRepository.Object, _mapper);
            _shapesRepository.Setup(x => x.Add(It.IsAny<Shape>())).Returns(1);
            Assert.Catch<Exception>(() => { shapeService.AddNewShape(null); });
        }

        [Test]
        public void AddNewShape_Given_Position0X0Y0Z_And_DimensionNull_And_ShapeTypeCube_Returns_Exception()
        {
            var cubeExerciseDbContextMoq = new Mock<IShapesRepository>();
            var shapeService = new ShapeService(cubeExerciseDbContextMoq.Object, _mapper);
            var shapeModel = new ShapeDtoSl
            {
                Position = new Position
                {
                    X = 0,
                    Y = 0,
                    Z = 0
                }
            };
            cubeExerciseDbContextMoq.Setup(x => x.Add(It.IsAny<Shape>())).Returns(1);
            Assert.Catch<Exception>(() => { shapeService.AddNewShape(shapeModel); });
        }

        [Test]
        public void AddNewShape_Given_Position0X0Y0Z_And_Dimensions10X10Z10Y_And_ShapeTypeCube_Returns_Int()
        {
            var cubeExerciseDbContextMoq = new Mock<IShapesRepository>();
            var shapeService = new ShapeService(cubeExerciseDbContextMoq.Object, _mapper);
            var shapeModel = new ShapeDtoSl
            {
                Position = new Position
                {
                    X = 0,
                    Y = 0,
                    Z = 0
                },
                Dimensions = new Dimensions
                {
                    Length = 10,
                    Height = 10,
                    Width = 10
                },
                ShapeType = ShapeTypes.Cube
            };
            cubeExerciseDbContextMoq.Setup(x => x.Add(It.IsAny<Shape>())).Returns(1);
            var result = shapeService.AddNewShape(shapeModel);
            Assert.AreEqual(1, result);
        }

        [Test]
        public void AddNewShape_Given_PositionDefault_And_DimensionsDefault_And_ShapeTypeDefaultValue_Returns_Int()
        {
            var cubeExerciseDbContextMoq = new Mock<IShapesRepository>();
            var shapeService = new ShapeService(cubeExerciseDbContextMoq.Object, _mapper);
            var shapeModel = new ShapeDtoSl
            {
                Position = new Position
                {
                    X = 0,
                    Y = 0,
                    Z = 0
                },
                Dimensions = new Dimensions
                {
                    Length = 10,
                    Height = 10,
                    Width = 10
                },
                ShapeType = default
            };
            cubeExerciseDbContextMoq.Setup(x => x.Add(It.IsAny<Shape>())).Returns(1);
            Assert.Catch<Exception>(() => { shapeService.AddNewShape(shapeModel); });
        }

        [Test]
        public void AddNewShape_Given_PositionNull_And_Dimension10X10Y10H_And_ShapeTypeCube_Returns_Exception()
        {
            var cubeExerciseDbContextMoq = new Mock<IShapesRepository>();
            var shapeService = new ShapeService(cubeExerciseDbContextMoq.Object, _mapper);
            var shapeModel = new ShapeDtoSl
            {
                Dimensions = new Dimensions
                {
                    Length = 10,
                    Height = 10,
                    Width = 10
                }
            };
            cubeExerciseDbContextMoq.Setup(x => x.Add(It.IsAny<Shape>())).Returns(1);
            Assert.Catch<Exception>(() => { shapeService.AddNewShape(shapeModel); });
        }
    }
}