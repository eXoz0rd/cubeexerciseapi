﻿using System;
using System.Collections.Generic;
using CubeExerciseCommon.Structs;
using CubeExerciseServiceLayer.Models;
using CubeExerciseServiceLayer.Models.AbstractModels;
using CubeExerciseServiceLayer.Models.ShapeModels;
using CubeExerciseServiceLayer.Services;
using FluentAssertions;
using NUnit.Framework;

namespace CubeExerciseUnitTests.SLUnitTests
{
    [TestFixture]
    public class CollisionDetectorUnitTests
    {
        [Test]
        public void Given_NewListOfShapeAbstract_When_DetectsCollision_Returns_EmptyListOfCollisions()
        {
            var model = new List<ShapeAbstract>();
            var expectedResult = new List<CollisionSl>();
            var collisionDetector = new CollisionDetector();
            var result = collisionDetector.DetectCollision(model);

            result.Should().BeEquivalentTo(expectedResult);
        }

        [Test]
        public void Given_Null_When_DetectsCollision_Returns_EmptyListOfCollisions()
        {
            var expectedResult = new List<CollisionSl>();
            var collisionDetector = new CollisionDetector();
            var result = collisionDetector.DetectCollision(null);

            result.Should().BeEquivalentTo(expectedResult);
        }

        [Test]
        public void Given_ListOfNulls_When_DetectsCollision_Returns_EmptyListOfCollisions()
        {
            var model = new List<ShapeAbstract>
            {
                null, null, null
            };
            var expectedResult = new List<CollisionSl>();
            var collisionDetector = new CollisionDetector();
            var result = collisionDetector.DetectCollision(model);

            result.Should().BeEquivalentTo(expectedResult);
        }

        [Test]
        public void Given_ListOfNullsAndOneGoodObject_When_DetectsCollision_Returns_EmptyListOfCollisions()
        {
            var model = new List<ShapeAbstract>
            {
                new CubeSl(new Position
                {
                    X = 0,
                    Y = 0,
                    Z = 0
                }, new Dimensions
                {
                    Height = 10,
                    Length = 10,
                    Width = 10
                }, new Guid()),
                null, null
            };
            var expectedResult = new List<CollisionSl>();
            var collisionDetector = new CollisionDetector();
            var result = collisionDetector.DetectCollision(model);

            result.Should().BeEquivalentTo(expectedResult);
        }

        [Test]
        public void Given_TwoShapeAbstractObjects_When_DetectsCollision_Returns_ListOfCollisionSlCount1()
        {
            var model = new List<ShapeAbstract>
            {
                new CubeSl(new Position
                {
                    X = 0,
                    Y = 0,
                    Z = 0
                }, new Dimensions
                {
                    Height = 10,
                    Length = 10,
                    Width = 10
                }, new Guid()),
                new CubeSl(new Position
                {
                    X = 5,
                    Y = 0,
                    Z = 0
                }, new Dimensions
                {
                    Height = 10,
                    Length = 10,
                    Width = 10
                }, new Guid()),
            };
            var expectedResult = new List<CollisionSl>
            {
                new CollisionSl
                {
                    FirstShape = model[0],
                    SecondShape = model[1]
                }
            };
            var collisionDetector = new CollisionDetector();
            var result = collisionDetector.DetectCollision(model);

            result.Should().BeEquivalentTo(expectedResult, opt => opt.Excluding(x => x.Id));
        }

        [Test]
        public void Given_ThreeShapeAbstractObjects_When_DetectsCollision_Returns_ListOfCollisionSlCount1()
        {
            var model = new List<ShapeAbstract>
            {
                new CubeSl(new Position
                {
                    X = 0,
                    Y = 0,
                    Z = 0
                }, new Dimensions
                {
                    Height = 10,
                    Length = 10,
                    Width = 10
                }, new Guid()),
                new CubeSl(new Position
                {
                    X = 5,
                    Y = 0,
                    Z = 0
                }, new Dimensions
                {
                    Height = 10,
                    Length = 10,
                    Width = 10
                }, new Guid()),
                new CubeSl(new Position
                {
                    X = 5,
                    Y = 5,
                    Z = 5
                }, new Dimensions
                {
                    Height = 5,
                    Length = 10,
                    Width = 10
                }, new Guid()),
            };
            var collisionDetector = new CollisionDetector();
            var expectedResult = new List<CollisionSl>
            {
                new CollisionSl
                {
                    FirstShape = model[0],
                    SecondShape = model[1],
                }
            };
            var result = collisionDetector.DetectCollision(model);

            result.Should().BeEquivalentTo(expectedResult, opt => opt.Excluding(x => x.Id));
        }

        [Test]
        public void Given_FourShapeAbstractObjects_When_DetectsCollision_Returns_ListOfCollisionSlCount2()
        {
            var model = new List<ShapeAbstract>
            {
                new CubeSl(new Position
                {
                    X = 0,
                    Y = 0,
                    Z = 0
                }, new Dimensions
                {
                    Height = 10,
                    Length = 10,
                    Width = 10
                }, new Guid()),
                new CubeSl(new Position
                {
                    X = 5,
                    Y = 0,
                    Z = 0
                }, new Dimensions
                {
                    Height = 10,
                    Length = 10,
                    Width = 10
                }, new Guid()),
                new CubeSl(new Position
                {
                    X = 10,
                    Y = 10,
                    Z = 10
                }, new Dimensions
                {
                    Height = 10,
                    Length = 10,
                    Width = 10
                }, new Guid()),
                new CubeSl(new Position
                {
                    X = 10,
                    Y = 10,
                    Z = 10
                }, new Dimensions
                {
                    Height = 5,
                    Length = 5,
                    Width = 5
                }, new Guid()),
            };
            var collisionDetector = new CollisionDetector();
            var expectedResult = new List<CollisionSl>
            {
                new CollisionSl
                {
                    FirstShape = model[0],
                    SecondShape = model[1],
                },
                new CollisionSl
                {
                    FirstShape = model[2],
                    SecondShape = model[3],
                }
            };
            var result = collisionDetector.DetectCollision(model);

            result.Should().BeEquivalentTo(expectedResult, opt => opt.Excluding(x => x.Id));
        }

        [Test]
        public void Given_FourSameShapeAbstractObjects_When_DetectsCollision_Returns_ListOfCollisionSlCount2()
        {
            var model = new List<ShapeAbstract>
            {
                new CubeSl(new Position
                {
                    X = 0,
                    Y = 0,
                    Z = 0
                }, new Dimensions
                {
                    Height = 10,
                    Length = 10,
                    Width = 10
                }, new Guid()),
                new CubeSl(new Position
                    {
                        X = 0,
                        Y = 0,
                        Z = 0
                    }, new Dimensions
                    {
                        Height = 10,
                        Length = 10,
                        Width = 10
                    },
                    new Guid()),
                new CubeSl(new Position
                    {
                        X = 0,
                        Y = 0,
                        Z = 0
                    }, new Dimensions
                    {
                        Height = 10,
                        Length = 10,
                        Width = 10
                    },
                    new Guid()),
                new CubeSl(new Position
                {
                    X = 0,
                    Y = 0,
                    Z = 0
                }, new Dimensions
                {
                    Height = 10,
                    Length = 10,
                    Width = 10
                }, new Guid()),
            };
            var collisionDetector = new CollisionDetector();
            var expectedResult = new List<CollisionSl>
            {
                new CollisionSl
                {
                    FirstShape = model[0],
                    SecondShape = model[1],
                },
                new CollisionSl
                {
                    FirstShape = model[2],
                    SecondShape = model[3],
                }
            };
            var result = collisionDetector.DetectCollision(model);

            result.Should().BeEquivalentTo(expectedResult, opt => opt.Excluding(x => x.Id));
        }
    }
}