﻿using System.Collections.Generic;
using CubeExerciseCommon.Enums;
using CubeExerciseCommon.Structs;
using CubeExerciseServiceLayer.Interfaces;
using CubeExerciseServiceLayer.Interfaces.Services;
using CubeExerciseServiceLayer.Models;
using CubeExerciseServiceLayer.Models.AbstractModels;
using CubeExerciseServiceLayer.Models.DTO;
using CubeExerciseServiceLayer.Models.ShapeModels;
using CubeExerciseServiceLayer.Services;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace CubeExerciseUnitTests.SLUnitTests
{
    [TestFixture]
    public class CollisionServiceUnitTests
    {
        private readonly Mock<ICollisionDetector> _collisionDetectorMock;
        private readonly Mock<IShapeService> _shapeServiceMock;

        public CollisionServiceUnitTests()
        {
            _collisionDetectorMock = new Mock<ICollisionDetector>();
            _shapeServiceMock = new Mock<IShapeService>();
        }

        private void SetUpShapeServiceMock()
        {
            _shapeServiceMock.Setup(x => x.GetAllShapes()).Returns(() => new List<ShapeDtoSl>
            {
                new ShapeDtoSl
                {
                    Dimensions = new Dimensions
                    {
                        Height = 10,
                        Length = 10,
                        Width = 10
                    },
                    Position = new Position
                    {
                        X = 0,
                        Y = 0,
                        Z = 0
                    },
                    ShapeType = ShapeTypes.Cube
                },
                new ShapeDtoSl
                {
                    Dimensions = new Dimensions
                    {
                        Height = 5,
                        Length = 5,
                        Width = 5
                    },
                    Position = new Position
                    {
                        X = 5,
                        Y = 0,
                        Z = 0
                    },
                    ShapeType = ShapeTypes.Cube
                },
                new ShapeDtoSl
                {
                    Dimensions = new Dimensions
                    {
                        Height = 5,
                        Length = 5,
                        Width = 5
                    },
                    Position = new Position
                    {
                        X = 20,
                        Y = 20,
                        Z = 20
                    },
                    ShapeType = ShapeTypes.Cube
                }
            });
        }


        [Test]
        public void Given_ListOfShapeAbstracts_When_DetectsCollision_Then_ReturnsListOfCollisionSl()
        {
            SetUpShapeServiceMock();
            _collisionDetectorMock.Setup(x => x.DetectCollision(It.IsAny<List<ShapeAbstract>>()))
                .Returns(() => new List<CollisionSl>());
            var collisionService = new CollisionService(_collisionDetectorMock.Object, _shapeServiceMock.Object);
            var result = collisionService.CalculateCollisions();
            var expectedResult = new List<CollisionSl>();
            Assert.AreEqual(expectedResult, result);
        }
    }
}