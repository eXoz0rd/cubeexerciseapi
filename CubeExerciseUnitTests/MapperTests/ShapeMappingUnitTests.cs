﻿using AutoMapper;
using CubeExerciseCommon.Enums;
using CubeExerciseCommon.Structs;
using CubeExerciseDAL.Models;
using CubeExerciseServiceLayer.Infrastructure;
using CubeExerciseServiceLayer.Models.DTO;
using CubeExerciseServiceLayer.Models.ShapeModels;
using FluentAssertions;
using NUnit.Framework;

namespace CubeExerciseUnitTests.MapperTests
{
    [TestFixture]
    public class ShapeMappingUnitTests
    {
        private readonly IMapper _mapper;

        public ShapeMappingUnitTests()
        {
            var myProfile = new SlMapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _mapper = new Mapper(configuration);
        }

        [Test]
        public void MappingCubeSlToShapes_Given_Position0X0Y0Z_And_Dimensions10X10Z10Y_Returns_ShapesObject()
        {
            var shapeModel = new ShapeDtoSl
            {
                Position = new Position
                {
                    X = 0,
                    Y = 0,
                    Z = 0
                },
                Dimensions = new Dimensions
                {
                    Length = 10,
                    Height = 10,
                    Width = 10
                },
                ShapeType = ShapeTypes.Cube
            };
            var expectedModel = new Shape
            {
                Position = new Position
                {
                    X = 0,
                    Y = 0,
                    Z = 0
                },
                Dimensions = new Dimensions
                {
                    Length = 10,
                    Height = 10,
                    Width = 10
                },
                ShapeType = ShapeTypes.Cube
            };
            var result = _mapper.Map<Shape>(shapeModel);

            result.Should().BeEquivalentTo(expectedModel);
        }
    }
}