﻿using System;
using CubeExerciseCommon.Enums;
using CubeExerciseCommon.Structs;

namespace CubeExerciseDAL.Models
{
    public class Shape
    {
        public Guid Id { get; set; }

        public Position Position { get; set; }

        public Dimensions Dimensions { get; set; }

        public ShapeTypes ShapeType { get; set; }
    }
}