﻿using CubeExerciseDAL.Models;
using Microsoft.EntityFrameworkCore;

namespace CubeExerciseDAL
{
    public class CubeExerciseDbContext : DbContext
    {
        public CubeExerciseDbContext(DbContextOptions<CubeExerciseDbContext> options) : base(options)
        {
        }

        public DbSet<Shape> Shapes { get; set; }
    }
}