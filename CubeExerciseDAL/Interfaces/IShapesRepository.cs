﻿using System;
using System.Collections.Generic;
using CubeExerciseDAL.Models;

namespace CubeExerciseDAL.Interfaces
{
    public interface IShapesRepository
    {
        int Add(Shape shapeModel);
        Shape GetById(Guid id);
        IEnumerable<Shape> GetAll();
        int Delete(Shape shape);
    }
}