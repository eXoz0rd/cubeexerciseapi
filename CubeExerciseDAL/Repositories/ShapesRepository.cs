﻿using System;
using System.Collections.Generic;
using System.Linq;
using CubeExerciseDAL.Interfaces;
using CubeExerciseDAL.Models;

namespace CubeExerciseDAL.Repositories
{
    public class ShapesRepository : IShapesRepository
    {
        private readonly CubeExerciseDbContext _cubeExerciseDbContext;

        public ShapesRepository(CubeExerciseDbContext cubeExerciseDbContext)
        {
            _cubeExerciseDbContext = cubeExerciseDbContext;
        }

        public int Add(Shape shapeModel)
        {
            _cubeExerciseDbContext.Shapes.Add(shapeModel);
           return _cubeExerciseDbContext.SaveChanges();
        }

        public Shape GetById(Guid id)
        {
            return _cubeExerciseDbContext.Set<Shape>().FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Shape> GetAll()
        {
            return _cubeExerciseDbContext.Shapes;
        }

        public int Delete(Shape shape)
        {
            _cubeExerciseDbContext.Set<Shape>().Remove(shape);
            return _cubeExerciseDbContext.SaveChanges();
        }
    }
}