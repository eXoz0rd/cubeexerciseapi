﻿using CubeExerciseDAL.Interfaces;
using CubeExerciseDAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace CubeExerciseDAL.Infrastructure
{
    public static class DalServiceInjector
    {
        public static void Configure(IServiceCollection serviceCollection)
        {
            MapServices(serviceCollection);
            AddDbContext(serviceCollection);
        }

        public static void MapServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IShapesRepository, ShapesRepository>();
        }


        private static void AddDbContext(IServiceCollection serviceCollection)
        {
            serviceCollection.AddDbContext<CubeExerciseDbContext>(opt => opt.UseInMemoryDatabase("CubeExercise"));
        }
    }
}