﻿using System;
using CubeExerciseCommon.Enums;
using CubeExerciseCommon.Structs;
using CubeExerciseServiceLayer.Models.AbstractModels;
using CubeExerciseServiceLayer.Models.ShapeModels;

namespace CubeExerciseServiceLayer.Factories
{
    public class ShapeFactory
    {
        public ShapeAbstract ResolveObject(ShapeTypes shapeType, Dimensions dimensions, Position position, Guid id)
        {
            if (shapeType == default) throw new Exception("Shape type undefined");

            switch (shapeType)
            {
                case ShapeTypes.Cube:
                    return new CubeSl(position, dimensions, id);
                default:
                    throw new Exception("Shape type is not supported.");
            }
        }
    }
}