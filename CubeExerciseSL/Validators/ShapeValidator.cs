﻿using CubeExerciseServiceLayer.Models.DTO;
using CubeExerciseServiceLayer.Models.ShapeModels;
using FluentValidation;

namespace CubeExerciseServiceLayer.Validators
{
    public class ShapeValidator : AbstractValidator<ShapeDtoSl>
    {
        public ShapeValidator()
        {
            RuleFor(x => x.Dimensions).NotEmpty()
                .WithMessage("Please enter correct dimensions.");

            RuleFor(x => x.ShapeType).NotEmpty().WithMessage("Please specify shape type");
        }
    }
}