﻿using System.Collections.Generic;
using CubeExerciseServiceLayer.Models;
using CubeExerciseServiceLayer.Models.AbstractModels;

namespace CubeExerciseServiceLayer.Interfaces
{
    public interface ICollisionDetector
    {
        List<CollisionSl> DetectCollision(List<ShapeAbstract> shapeAbstracts);
    }
}