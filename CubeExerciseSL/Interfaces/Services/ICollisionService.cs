﻿using System.Collections.Generic;
using CubeExerciseServiceLayer.Models;

namespace CubeExerciseServiceLayer.Interfaces.Services
{
    public interface ICollisionService
    {
        public List<CollisionSl> CalculateCollisions();
    }
}