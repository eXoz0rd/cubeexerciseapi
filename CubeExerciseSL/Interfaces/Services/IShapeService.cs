﻿using System.Collections.Generic;
using CubeExerciseServiceLayer.Models.DTO;
using CubeExerciseServiceLayer.Models.ShapeModels;

namespace CubeExerciseServiceLayer.Interfaces.Services
{
    public interface IShapeService
    {
        int AddNewShape(ShapeDtoSl shapeDtoAbstract);

        List<ShapeDtoSl> GetAllShapes();
    }
}