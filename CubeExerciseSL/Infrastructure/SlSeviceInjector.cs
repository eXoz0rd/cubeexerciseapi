﻿using CubeExerciseDAL.Infrastructure;
using CubeExerciseServiceLayer.Interfaces;
using CubeExerciseServiceLayer.Interfaces.Services;
using CubeExerciseServiceLayer.Services;
using Microsoft.Extensions.DependencyInjection;

namespace CubeExerciseServiceLayer.Infrastructure
{
    public static class SlServiceInjector
    {
        public static void Configure(IServiceCollection serviceCollection)
        {
            MapServices(serviceCollection);
            DalServiceInjector.Configure(serviceCollection);
        }

        private static void MapServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IShapeService, ShapeService>();
            serviceCollection.AddTransient<ICollisionService, CollisionService>();
            serviceCollection.AddTransient<ICollisionDetector, CollisionDetector>();
        }
    }
}