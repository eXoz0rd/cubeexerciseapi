﻿using AutoMapper;
using CubeExerciseDAL.Models;
using CubeExerciseServiceLayer.Models.DTO;
using CubeExerciseServiceLayer.Models.ShapeModels;

namespace CubeExerciseServiceLayer.Infrastructure
{
    public class SlMapperProfile : Profile
    {
        public SlMapperProfile()
        {
            RegisterMappings();
        }

        private void RegisterMappings()
        {
            CreateMap<ShapeDtoSl, Shape>().ReverseMap();
        }
    }
}