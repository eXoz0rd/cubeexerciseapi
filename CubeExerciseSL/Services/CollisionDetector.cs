﻿using System.Collections.Generic;
using System.Linq;
using CubeExerciseBLL;
using CubeExerciseServiceLayer.Interfaces;
using CubeExerciseServiceLayer.Models;
using CubeExerciseServiceLayer.Models.AbstractModels;

namespace CubeExerciseServiceLayer.Services
{
    /// <summary>
    ///     Module that detects collision between objects
    /// </summary>
    public class CollisionDetector : ICollisionDetector
    {
        public List<CollisionSl> DetectCollision(List<ShapeAbstract> shapeAbstracts)
        {
            if (shapeAbstracts == null || shapeAbstracts.Count == 0) return new List<CollisionSl>();

            var collisions = new List<CollisionSl>();
            foreach (var newCollision in shapeAbstracts.SelectMany(firstShape =>
                from secondShape in shapeAbstracts
                where firstShape != secondShape && firstShape != null && secondShape != null
                where !collisions.Any(x => x.FirstShape == firstShape || x.SecondShape == secondShape) &&
                      !collisions.Any(x => x.FirstShape == secondShape || x.SecondShape == firstShape)
                where firstShape.AreColliding(secondShape)
                select new CollisionSl
                {
                    FirstShape = firstShape,
                    SecondShape = secondShape
                }))
            {
                collisions.Add(newCollision);
            }

            return collisions;
        }
    }
}