﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CubeExerciseDAL.Interfaces;
using CubeExerciseDAL.Models;
using CubeExerciseServiceLayer.Interfaces.Services;
using CubeExerciseServiceLayer.Models.DTO;
using CubeExerciseServiceLayer.Models.ShapeModels;
using CubeExerciseServiceLayer.Validators;

namespace CubeExerciseServiceLayer.Services
{
    public class ShapeService : IShapeService
    {
        private readonly IMapper _mapper;
        private readonly IShapesRepository _shapesRepository;
        private readonly ShapeValidator _shapeValidator;

        public ShapeService(IShapesRepository shapesRepository, IMapper mapper)
        {
            _shapesRepository = shapesRepository;
            _mapper = mapper;
            _shapeValidator = new ShapeValidator();
        }

        public int AddNewShape(ShapeDtoSl shapeDtoSl)
        {
            if (shapeDtoSl == null)
                throw new Exception("There was a problem with adding your shape, please try again.");

            var validationResult = _shapeValidator.Validate(shapeDtoSl);

            if (!validationResult.IsValid)
            {
                //TODO: Refactor this into more Abstract/generic way - maybe string[] extension?
                var aggregatedErrors = validationResult.Errors.Select(x => x.ErrorMessage)
                    .Aggregate((prev, next) => $"{prev}{Environment.NewLine}{next}");
                throw new Exception(
                    $"Errors: {aggregatedErrors}");
            }

            var mappedEntity = _mapper.Map<Shape>(shapeDtoSl);

            if (mappedEntity == null) throw new Exception("There was a problem with mapping, please try again.");

            return _shapesRepository.Add(mappedEntity);
        }

        public List<ShapeDtoSl> GetAllShapes()
        {
            var shapes = _shapesRepository.GetAll();
            return _mapper.Map<List<ShapeDtoSl>>(shapes);
        }
    }
}