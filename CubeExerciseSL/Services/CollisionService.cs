﻿using System.Collections.Generic;
using System.Linq;
using CubeExerciseServiceLayer.Factories;
using CubeExerciseServiceLayer.Interfaces;
using CubeExerciseServiceLayer.Interfaces.Services;
using CubeExerciseServiceLayer.Models;
using CubeExerciseServiceLayer.Models.AbstractModels;

namespace CubeExerciseServiceLayer.Services
{
    public class CollisionService : ICollisionService
    {
        private readonly ICollisionDetector _collisionDetector;

        //TODO Remove services coupling - There is possibility to use observer pattern to trigger changes while adding new Shape
        //TODO then we could store data in memory (cache) and re-calculate it on fly.
        private readonly IShapeService _shapeService;

        public CollisionService(ICollisionDetector collisionDetector, IShapeService shapeService)
        {
            _collisionDetector = collisionDetector;
            _shapeService = shapeService;
        }

        public List<CollisionSl> CalculateCollisions()
        {
            var allCurrentShapes = _shapeService.GetAllShapes();
            var factory = new ShapeFactory();
            var shapeAbstracts =
                allCurrentShapes.Select(x => factory.ResolveObject(x.ShapeType, x.Dimensions, x.Position, x.Id)).ToList();
            
            return _collisionDetector.DetectCollision(shapeAbstracts);
        }
    }
}