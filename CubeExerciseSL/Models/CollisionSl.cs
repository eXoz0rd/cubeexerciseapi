﻿using System;
using CubeExerciseBLL;
using CubeExerciseServiceLayer.Models.AbstractModels;

namespace CubeExerciseServiceLayer.Models
{
    public class CollisionSl
    {
        //Just random Guid
        public Guid Id => Guid.NewGuid();
        public ShapeAbstract FirstShape { get; set; }

        public ShapeAbstract SecondShape { get; set; }

        public double IntersectionArea => CountIntersectionArea();

        private double CountIntersectionArea()
        {
            var length = FirstShape.PositionRange.MinVector.X + SecondShape.PositionRange.MaxVector.X;
            return CubeEquations.CalculateVolume(length);
        }
    }
}