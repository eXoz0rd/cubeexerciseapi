﻿using System;
using CubeExerciseCommon.Enums;
using CubeExerciseCommon.Structs;

namespace CubeExerciseServiceLayer.Models.DTO
{
    /// <summary>
    /// DTO/POCO model of Shape that transfers data between layers
    /// </summary>
    public class ShapeDtoSl
    {
        public Guid Id { get; set; }
        
        public Dimensions Dimensions { get; set; }

        public Position Position { get; set; }

        public ShapeTypes ShapeType { get; set; }
    }
}