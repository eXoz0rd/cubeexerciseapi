﻿using System;
using CubeExerciseBLL;
using CubeExerciseCommon.Enums;
using CubeExerciseCommon.Structs;
using CubeExerciseServiceLayer.Models.AbstractModels;

namespace CubeExerciseServiceLayer.Models.ShapeModels
{
    /// <summary>
    /// Cube model
    /// </summary>
    /// 
    //It's not typical N-layered object (POCO Object) i wanted to go more into DDD like object
    //where it contains more than just plain properties and has some logic in it.
    public class CubeSl : ShapeAbstract
    {
        public CubeSl(Position position, Dimensions dimensions, Guid id) : base(position, dimensions, id)
        {
        }

        public override ShapeTypes ShapeType { get; } = ShapeTypes.Cube;
        public override double SurfaceArea => CalculateSurfaceArea();

        public override double Volume => CalculateVolume();

        public override PositionRange PositionRange => CalculatePositionRange();

        protected internal override double CalculateSurfaceArea()
        {
            return CubeEquations.CalculateSurfaceArea(Dimensions.Length);
        }

        protected internal override double CalculateVolume()
        {
            return CubeEquations.CalculateVolume(Dimensions.Length);
        }

        //TODO Refactor
        protected internal override PositionRange CalculatePositionRange()
        {
            var maxX = Position.X + Dimensions.Length / 2;
            var maxY = Position.Y + Dimensions.Height / 2;
            var maxZ = Position.Z + Dimensions.Width / 2;

            var minX = Position.X - Dimensions.Length / 2;
            var minY = Position.Y - Dimensions.Height / 2;
            var minZ = Position.Z - Dimensions.Width / 2;

            return new PositionRange
            {
                MaxVector = new Vector
                {
                    X = maxX,
                    Y = maxY,
                    Z = maxZ
                },
                MinVector = new Vector
                {
                    X = minX,
                    Y = minY,
                    Z = minZ
                }
            };
        }
    }
}