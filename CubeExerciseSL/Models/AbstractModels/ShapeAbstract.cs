﻿using System;
using CubeExerciseCommon.Enums;
using CubeExerciseCommon.Structs;

namespace CubeExerciseServiceLayer.Models.AbstractModels
{
    public abstract class ShapeAbstract
    {
        public Guid Id { get; protected set; }
        
        /// <summary>
        /// Determines Position of the object center
        /// </summary>
        public Position Position { get; set; }

        /// <summary>
        /// Determines object dimensions
        /// </summary>
        public Dimensions Dimensions { get; set; }

        public abstract PositionRange PositionRange { get; }

        public abstract ShapeTypes ShapeType { get; }

        public abstract double SurfaceArea { get; }

        public abstract double Volume { get; }
        
        
        protected ShapeAbstract(Position position, Dimensions dimensions, Guid id)
        {
            Position = position;
            Dimensions = dimensions;
            Id = id;
        }

        protected internal abstract double CalculateSurfaceArea();

        protected internal abstract double CalculateVolume();

        protected internal abstract PositionRange CalculatePositionRange();

        public bool AreColliding(ShapeAbstract secondObject)
        {
            return PositionRange.IsInRange(secondObject.PositionRange);
        }
    }
}