﻿using System;
using CubeExerciseCommon.Interfaces;

namespace CubeExerciseCommon.Structs
{
    public struct Dimensions : IDimensions
    {
        public double Length { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
    }
}