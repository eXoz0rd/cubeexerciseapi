﻿namespace CubeExerciseCommon.Structs
{
    public struct PositionRange
    {
        /// <summary>
        /// Vector that determines Max of our shape in 3d World
        /// </summary>
        public Vector MaxVector { get; set; }

        /// <summary>
        /// Vector that determines Min of our shape in 3d World
        /// </summary>
        public Vector MinVector { get; set; }

        /// <summary>
        /// Determines whether two positionRanges are intersecting
        /// </summary>
        /// <param name="positionRange"></param>
        /// <returns></returns>
        public bool IsInRange(PositionRange positionRange)
        {
            if (!(MinVector.X < positionRange.MaxVector.X) && !(MaxVector.X > positionRange.MinVector.X)) return false;
            if (!(MinVector.Y < positionRange.MaxVector.Y) && !(MaxVector.Y > positionRange.MinVector.Y)) return false;
            if (!(MinVector.Z < positionRange.MaxVector.Z) && !(MaxVector.Z > positionRange.MinVector.Z)) return false;

            return true;
        }
    }
}