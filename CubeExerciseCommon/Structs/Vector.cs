﻿using CubeExerciseCommon.Interfaces;

namespace CubeExerciseCommon.Structs
{
    public class Vector : IPositionCoordinates
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
    }
}