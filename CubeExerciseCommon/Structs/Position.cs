﻿using CubeExerciseCommon.Interfaces;

namespace CubeExerciseCommon.Structs
{
    public struct Position : IPositionCoordinates
    {
        /// <summary>
        /// Determines Center of Shape
        /// </summary>
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
    }
}