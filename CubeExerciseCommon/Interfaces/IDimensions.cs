﻿namespace CubeExerciseCommon.Interfaces
{
    /// <summary>
    /// Standard 3D Dimensions interface
    /// </summary>
    public interface IDimensions
    {
        public double Length { get; set; }

        public double Width { get; set; }

        public double Height { get; set; }
    }
}