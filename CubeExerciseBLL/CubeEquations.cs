﻿using System;

namespace CubeExerciseBLL
{
    public static class CubeEquations
    {
        public static double CalculateSurfaceArea(double length)
        {
            return 6 * Math.Pow(length, 2);
        }

        public static double CalculateVolume(double length)
        {
            return Math.Pow(length, 3);
        }
    }
}