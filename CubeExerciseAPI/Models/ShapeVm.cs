﻿using System;
using CubeExerciseCommon.Enums;
using CubeExerciseCommon.Structs;

namespace CubeExerciseAPI.Models
{
    public class ShapeVm
    {
        public Guid? Id { get; set; }
        
        public Dimensions Dimensions { get; set; }

        public Position Position { get; set; }

        public ShapeTypes ShapeType { get; set; }
    }
}