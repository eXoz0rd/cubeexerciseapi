﻿using AutoMapper;
using CubeExerciseAPI.Models;
using CubeExerciseServiceLayer.Interfaces.Services;
using CubeExerciseServiceLayer.Models.DTO;
using CubeExerciseServiceLayer.Models.ShapeModels;
using Microsoft.AspNetCore.Mvc;

namespace CubeExerciseAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ShapeController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IShapeService _shapeService;

        public ShapeController(IShapeService shapeService, IMapper mapper)
        {
            _shapeService = shapeService;
            _mapper = mapper;
        }

        [HttpGet("ReadAll")]
        public JsonResult ReadAll()
        {
            var result = _shapeService.GetAllShapes();
            return new JsonResult(result);
        }

        [HttpPost("Create")]
        public JsonResult Create([FromBody] ShapeVm shapeVm)
        {
            var mappedShapeModel = _mapper.Map<ShapeDtoSl>(shapeVm);
            var result = _shapeService.AddNewShape(mappedShapeModel);
            return new JsonResult(result);
        }
    }
}