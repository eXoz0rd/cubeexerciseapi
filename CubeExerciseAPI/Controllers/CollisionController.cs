﻿using CubeExerciseServiceLayer.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace CubeExerciseAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CollisionController
    {

        private readonly ICollisionService _collisionService;
        
        public CollisionController(ICollisionService collisionService)
        {
            _collisionService = collisionService;
        }
        
        [HttpGet("CalculateCollisions")]
        public JsonResult CalculateCollisions()
        {
            var result = _collisionService.CalculateCollisions();
            return new JsonResult(result);
        }
    }
}