﻿using AutoMapper;
using CubeExerciseDAL.Infrastructure;
using CubeExerciseServiceLayer.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace CubeExerciseAPI.Infrastructure
{
    public static class ApiServiceInjector
    {
        public static void Configure(IServiceCollection serviceCollection)
        {
            ConfigureMapper(serviceCollection);
            SlServiceInjector.Configure(serviceCollection);
        }

        private static void ConfigureMapper(IServiceCollection serviceCollection)
        {
            var mapperConfig = new MapperConfiguration(config =>
            {
                config.AddProfile<SlMapperProfile>();
                config.AddProfile<ApiMapperProfile>();
            });
            var mapper = mapperConfig.CreateMapper();
            serviceCollection.AddSingleton(mapper);
        }
    }
}