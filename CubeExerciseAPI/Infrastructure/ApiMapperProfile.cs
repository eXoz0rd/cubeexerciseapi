﻿using AutoMapper;
using CubeExerciseAPI.Models;
using CubeExerciseServiceLayer.Models.DTO;
using CubeExerciseServiceLayer.Models.ShapeModels;

namespace CubeExerciseAPI.Infrastructure
{
    public class ApiMapperProfile : Profile
    {
        public ApiMapperProfile()
        {
            RegisterMappings();
        }

        private void RegisterMappings()
        {
            CreateMap<ShapeVm, ShapeDtoSl>().ReverseMap();
        }
    }
}